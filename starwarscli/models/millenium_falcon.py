# coding: utf-8

"""
    Star Wars

    This is an API which assists in calculating the probability that the Millenium Falcon will survive! Please visit `http://<host>:8080/ui/` for documentation.  # noqa: E501

    OpenAPI spec version: 1.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six


class MilleniumFalcon(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'autonomy': 'int',
        'departure': 'str',
        'arrival': 'str',
        'routes_db': 'str'
    }

    attribute_map = {
        'autonomy': 'autonomy',
        'departure': 'departure',
        'arrival': 'arrival',
        'routes_db': 'routes_db'
    }

    def __init__(self, autonomy=None, departure=None, arrival=None, routes_db=None):  # noqa: E501
        """MilleniumFalcon - a model defined in Swagger"""  # noqa: E501
        self._autonomy = None
        self._departure = None
        self._arrival = None
        self._routes_db = None
        self.discriminator = None
        if autonomy is not None:
            self.autonomy = autonomy
        if departure is not None:
            self.departure = departure
        if arrival is not None:
            self.arrival = arrival
        if routes_db is not None:
            self.routes_db = routes_db

    @property
    def autonomy(self):
        """Gets the autonomy of this MilleniumFalcon.  # noqa: E501


        :return: The autonomy of this MilleniumFalcon.  # noqa: E501
        :rtype: int
        """
        return self._autonomy

    @autonomy.setter
    def autonomy(self, autonomy):
        """Sets the autonomy of this MilleniumFalcon.


        :param autonomy: The autonomy of this MilleniumFalcon.  # noqa: E501
        :type: int
        """

        self._autonomy = autonomy

    @property
    def departure(self):
        """Gets the departure of this MilleniumFalcon.  # noqa: E501


        :return: The departure of this MilleniumFalcon.  # noqa: E501
        :rtype: str
        """
        return self._departure

    @departure.setter
    def departure(self, departure):
        """Sets the departure of this MilleniumFalcon.


        :param departure: The departure of this MilleniumFalcon.  # noqa: E501
        :type: str
        """

        self._departure = departure

    @property
    def arrival(self):
        """Gets the arrival of this MilleniumFalcon.  # noqa: E501


        :return: The arrival of this MilleniumFalcon.  # noqa: E501
        :rtype: str
        """
        return self._arrival

    @arrival.setter
    def arrival(self, arrival):
        """Sets the arrival of this MilleniumFalcon.


        :param arrival: The arrival of this MilleniumFalcon.  # noqa: E501
        :type: str
        """

        self._arrival = arrival

    @property
    def routes_db(self):
        """Gets the routes_db of this MilleniumFalcon.  # noqa: E501


        :return: The routes_db of this MilleniumFalcon.  # noqa: E501
        :rtype: str
        """
        return self._routes_db

    @routes_db.setter
    def routes_db(self, routes_db):
        """Sets the routes_db of this MilleniumFalcon.


        :param routes_db: The routes_db of this MilleniumFalcon.  # noqa: E501
        :type: str
        """

        self._routes_db = routes_db

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(MilleniumFalcon, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, MilleniumFalcon):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
