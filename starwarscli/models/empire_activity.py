# coding: utf-8

"""
    Star Wars

    This is an API which assists in calculating the probability that the Millenium Falcon will survive! Please visit `http://<host>:8080/ui/` for documentation.  # noqa: E501

    OpenAPI spec version: 1.0.0
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six


class EmpireActivity(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'bounty_hunters': 'list[BountyHunter]',
        'countdown': 'int'
    }

    attribute_map = {
        'bounty_hunters': 'bounty_hunters',
        'countdown': 'countdown'
    }

    def __init__(self, bounty_hunters=None, countdown=None):  # noqa: E501
        """EmpireActivity - a model defined in Swagger"""  # noqa: E501
        self._bounty_hunters = None
        self._countdown = None
        self.discriminator = None
        if bounty_hunters is not None:
            self.bounty_hunters = bounty_hunters
        if countdown is not None:
            self.countdown = countdown

    @property
    def bounty_hunters(self):
        """Gets the bounty_hunters of this EmpireActivity.  # noqa: E501


        :return: The bounty_hunters of this EmpireActivity.  # noqa: E501
        :rtype: list[BountyHunter]
        """
        return self._bounty_hunters

    @bounty_hunters.setter
    def bounty_hunters(self, bounty_hunters):
        """Sets the bounty_hunters of this EmpireActivity.


        :param bounty_hunters: The bounty_hunters of this EmpireActivity.  # noqa: E501
        :type: list[BountyHunter]
        """

        self._bounty_hunters = bounty_hunters

    @property
    def countdown(self):
        """Gets the countdown of this EmpireActivity.  # noqa: E501


        :return: The countdown of this EmpireActivity.  # noqa: E501
        :rtype: int
        """
        return self._countdown

    @countdown.setter
    def countdown(self, countdown):
        """Sets the countdown of this EmpireActivity.


        :param countdown: The countdown of this EmpireActivity.  # noqa: E501
        :type: int
        """

        self._countdown = countdown

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(EmpireActivity, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, EmpireActivity):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
