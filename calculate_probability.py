from __future__ import print_function
import time
import starwarscli
from starwarscli.rest import ApiException, RESTResponse
from starwarscli.configuration import Configuration
from starwarscli.models import EmpireActivity, MilleniumFalcon, EmpireActivityAndMilleniumFalcon, BountyHunter, Survivability
from pprint import pprint
import sys, json

STAR_WARS_API_BASE_URL = 'http://localhost:8080/'

#checks that the given Empire is valid (populated)
def empire_model_is_valid(empire):
    if empire.countdown is None or empire.bounty_hunters is None:
        return False
    return True

#checks that the given MilleniumFalcon is valid (populated)
def millenium_falcon_model_is_valid(falcon):
    if not falcon.autonomy or not falcon.departure or not falcon.arrival or not falcon.routes_db:
        return False
    return True

#checks that the given EmpireAndMilleniumFalcon is valid (populated)
#and that their child models are valid too
def empire_and_mf_model_is_valid(empire_and_mf):
    empire = empire_and_mf.empire_activity
    if not empire or not empire_model_is_valid(empire):
        return False

    millenium_falcon = empire_and_mf.millenium_falcon
    if not millenium_falcon or not millenium_falcon_model_is_valid(millenium_falcon):
        return False
    return True

def print_usage():
    print("USAGE: python3 calculate_probability.py <millenium_falcon_file> <empire_file>")

#validate input
args = sys.argv
if len(args) is not 3:
    print("ERROR: Missing required parameters")
    print_usage()
    sys.exit(1)

millenium_falcon_file_path = args[1]
empire_file_path = args[2]

try:
    with open(millenium_falcon_file_path) as mf_file:
        obj = json.load(mf_file)
        millenium_falcon =  starwarscli.ApiClient().deserialize_model(obj, "MilleniumFalcon")
except Exception as e:
    print("ERROR: There was a problem processing the Millenium Falcon file")
    sys.exit(1)

if not millenium_falcon_model_is_valid(millenium_falcon):
    print("ERROR: The given Millenium Falcon file is invalid")
    sys.exit(1)


try:
    with open(empire_file_path) as empire_file:
        obj = json.load(empire_file)
        empire = starwarscli.ApiClient().deserialize_model(obj, "EmpireActivity")
except Exception as e:
    print("ERROR: There was a problem processing the Empire file")
    sys.exit(1)

if not empire_model_is_valid(empire):
    print("ERROR: The given EmpireActivity file is invalid")
    sys.exit(1)

#input is valid, continue

#create configuration for StarWars API Client
configuration = Configuration()
configuration.host = STAR_WARS_API_BASE_URL

# create an instance of the API class
api_instance = starwarscli.DefaultApi(starwarscli.ApiClient(configuration))

#join the given Empire and MilleniumFalcon info into one object for transfer
empire_and_mf = EmpireActivityAndMilleniumFalcon(empire, millenium_falcon) 

try:
    # The probability of survival using a custom config.

    survivability = api_instance.survivability_post(body=empire_and_mf)
    probability_as_int = int(float(survivability.probability))
    print(probability_as_int)
except ApiException as e:
    print("Exception when calling DefaultApi->survivability_post: %s\n" % e)
