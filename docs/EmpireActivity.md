# EmpireActivity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bounty_hunters** | [**list[BountyHunter]**](BountyHunter.md) |  | [optional] 
**countdown** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

