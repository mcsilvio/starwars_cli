# EmpireActivityAndMilleniumFalcon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**empire_activity** | [**EmpireActivity**](EmpireActivity.md) |  | [optional] 
**millenium_falcon** | [**MilleniumFalcon**](MilleniumFalcon.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

