# starwarscli.DefaultApi

All URIs are relative to */*

Method | HTTP request | Description
------------- | ------------- | -------------
[**survivability_default_config_post**](DefaultApi.md#survivability_default_config_post) | **POST** /survivability/default_config | The probability of survival using default config.
[**survivability_post**](DefaultApi.md#survivability_post) | **POST** /survivability | The probability of survival using a custom config.

# **survivability_default_config_post**
> Survivability survivability_default_config_post(body=body)

The probability of survival using default config.

This endoint will return a probability that the Milleniam Falcon will survive. It uses the default configuration, and the given actions of the Empire to perform this calculation.

### Example
```python
from __future__ import print_function
import time
import starwarscli
from starwarscli.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = starwarscli.DefaultApi()
body = starwarscli.EmpireActivity() # EmpireActivity | An object that describes the activity of the Empire's bounty hunters. (optional)

try:
    # The probability of survival using default config.
    api_response = api_instance.survivability_default_config_post(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->survivability_default_config_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EmpireActivity**](EmpireActivity.md)| An object that describes the activity of the Empire&#x27;s bounty hunters. | [optional] 

### Return type

[**Survivability**](Survivability.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **survivability_post**
> Survivability survivability_post(body=body)

The probability of survival using a custom config.

This endoint will return a probability that the Milleniam Falcon will survive. It uses the given configuration, and the given actions of the Empire to perform this calculation.

### Example
```python
from __future__ import print_function
import time
import starwarscli
from starwarscli.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = starwarscli.DefaultApi()
body = starwarscli.EmpireActivityAndMilleniumFalcon() # EmpireActivityAndMilleniumFalcon | An object that describes the activity of the Empire's bounty hunters. (optional)

try:
    # The probability of survival using a custom config.
    api_response = api_instance.survivability_post(body=body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->survivability_post: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**EmpireActivityAndMilleniumFalcon**](EmpireActivityAndMilleniumFalcon.md)| An object that describes the activity of the Empire&#x27;s bounty hunters. | [optional] 

### Return type

[**Survivability**](Survivability.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

