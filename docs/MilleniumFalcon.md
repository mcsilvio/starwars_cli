# MilleniumFalcon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**autonomy** | **int** |  | [optional] 
**departure** | **str** |  | [optional] 
**arrival** | **str** |  | [optional] 
**routes_db** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

