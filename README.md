# Star Wars Survivability Calculator - Command-Line Interface
This is an API command-line client which assists in calculating the probability that the Millenium Falcon will survive its journey! 
It processes the input data and sends it to the Survivability Calculator API (https://bitbucket.org/mcsilvio/starwars_api/).

## Requirements.

- Python 3.4+
- Virtualenv
- pip3

## Tech Stack Used

- python3
- swagger generated client for Survivability Calculator API (swagger spec found in API project)

## Installation & Usage

Perform the following steps to install and use the program.  

1. Clone the repository to a local folder
2. From that folder, set up a virtual python environment by running   
`virtualenv -p python3 env && source env/bin/activate`
3. Install the required dependencies by running  
`pip3 install -r requirements.txt`
4. Configure the base url of the Survivability Calculator API by setting the following value at the top of the `calculate_probabilities` script.  
`STAR_WARS_API_BASE_URL = 'http://localhost:8080/'`
5. Run the `calculate_probabilities.py` script. (See Usage)

## Usage

The `calculate_probabilities` script has the following usage:  
`python3 calculate_probabilities.py <millenium_falcon_file> <empire_activity_file>`

### Output
The script will output a single number. This number is the percentage probability that the Millenium Falcon will survive its journey to its destination.

## Author

Silvio Marco Costantini


